const mongodb = require('mongodb')
const keys = require('../../config/keys')
var ObjectId = require('mongodb').ObjectID;
var _db, MultiDBs = [], connectionList = [];

// Primary Connection
var connectToDB = () => {
  const mongoClients = new mongodb.MongoClient(keys.mongoURI, { useNewUrlParser: true });
  mongoClients.connect(err => {
    //console.log("Main DataBase Connected => " + keys.mongoURI)
    _db = mongoClients.db('flowngin-dev');
    //if (!err) connectionEstablishment();
    //return callback(err)
  })
}

// Get Primary Connection
var getDB = () => {
  return _db;
}

// Connect secondary Database connection
var newconnectToDB = (host_name, mongoURI, callback) => {
  const mongoClient = new mongodb.MongoClient(mongoURI, { useNewUrlParser: true });
  mongoClient.connect(err => {
    if (err) callback(false);
    if (!err) {
      var db_name = mongoURI.split("/")[3];
      //console.log("connected to host name " + mongoURI)
      DisconnectionMongoURL(host_name);
      var obj = {
        _db: mongoClient.db(db_name),
        host_name: host_name,
        mongoURI: mongoURI
      }
      connectionList.push({
        host_name: host_name,
        mongoURI: mongoURI
      })
      MultiDBs.push(obj)
      return callback(obj)
    }
  });
}

// Connect secondary Database connection
var authConnection = (host_name, mongoURI) => {
  var obj = MultiDBs.find(o => o.host_name == host_name);
  if (obj) {
    return obj._db
  } else {
    newconnectToDB(host_name, mongoURI, (data) => {
      return data.host_name
    });
  }
}

// All secondary connection establishment
var connectionEstablishment = () => {
  const company = getDB().collection('companies');
  company.find({}).toArray((err, user_data) => {
    if (user_data.length != 0) {
      MultiDBs = [];
      user_data.forEach(element => {
        if (element.hostname != "portal") {
          authConnection(element.hostname, element.database_url);
        }
      });
    }
  });
}

// Get secondary connection 
var getDBs = (host_name) => {
  var obj = MultiDBs.find(o => o.host_name == host_name);
  if (obj) {
    return obj._db
  } else {
    return false
  }
}

var getConnectionList = () => {
  return connectionList
}

var connectDB = (host_name, db_name) => {
  var my_db;
  var obj = MultiDBs.find(o => o.host_name == host_name);
  if (obj) {
    try {
      my_db = getDBs(host_name).collection(db_name)
    } catch (e) {
      my_db = false
    }
  } else {
    my_db = false
  }
  return my_db;
}

var CheckValidateConnectionURL = (database_url, callback) => {
  //console.log(database_url)
  const mongoClient = new mongodb.MongoClient(database_url, { useNewUrlParser: true });
  mongoClient.connect(err => {
    if (err) {
      callback({ res: "1", message: "Validate Connection URL Error" });
    } else {
      try {
        var db_name = database_url.split("/")[3];
        if (db_name) {
          var User = mongoClient.db(db_name).collection('users');
          if (User) {
            callback({ res: "0", message: "Validate Connection Successfully" });
          } else {
            callback({ res: "1", message: "Validate Connection URL Error" });
          }
        } else {
          callback({ res: "1", message: "Validate Connection URL Error" });
        }
      } catch (e) {
        callback({ res: "1", message: "Validate Connection URL Error" });
      }

    }
  });
}

var DisconnectionMongoURL = (host_name) => {
  const filtered = MultiDBs.filter((item) => item.host_name != host_name);
  const filtered2 = connectionList.filter((item) => item.host_name != host_name);
  MultiDBs = filtered;
  connectionList = filtered2;
}

module.exports = {
  connectToDB: connectToDB,
  getDB: getDB,
  authConnection: authConnection,
  getDBs: getDBs,
  connectionEstablishment: connectionEstablishment,
  getConnectionList: getConnectionList,
  connectDB: connectDB,
  newconnectToDB: newconnectToDB,
  CheckValidateConnectionURL: CheckValidateConnectionURL,
  DisconnectionMongoURL: DisconnectionMongoURL,
  ObjectId : ObjectId
}
