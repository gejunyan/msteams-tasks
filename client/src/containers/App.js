import React, { Component } from "react";
import Routes from "../components/Routes/Routes";
import PrivateRoute from "../components/Routes/PrivateRoute";
import './App.css';

class App extends Component {
  render() {
    const { location } = this.props;
    return (
      <div className={"flyout"}>
      <PrivateRoute location={location} path="/" component={Routes} />
      </div >
    );
  }
}


export default App

