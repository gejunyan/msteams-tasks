import axios from 'axios';
import {showLoader} from './helperFunctions';

var config = {  timeout: 10000, headers: { "Authorization": "Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0" } };

var setAuthToken = () => {
  if (localStorage.access_token) {
    config.headers.Authorization = "Bearer " + localStorage.access_token;
  }
}

var resetAuthToken = () => {
  config.headers.Authorization = "Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0";
}


var axiosservice = (method, url, paylaod) => {
  console.log("call new axios" + url);
  showLoader(1)
  return new Promise((resolve, rejects) => {
    switch (method) {
      case 'POST':
        axios.post(url, paylaod, config).then((res) => {
          resolve(authorization(res.data));
        }).catch(err=>{
           showLoader(0);
          //unauthorization();
        })
        break;
      case 'GET':
        axios.get(url, config).then(res => {
          resolve(authorization(res.data))
        }).catch(err=>{
          showLoader(0);
          
          //unauthorization();
        });
        break;
      case 'PUT':
        axios.put(url, paylaod, config).then(res => {
          resolve(authorization(res.data))
        }).catch(err=>{
          showLoader(0)
          //unauthorization();
        });
        break;
      case 'DELETE':
        axios.delete(url, config).then(res => {
          resolve(authorization(res.data))
        }).catch(err=>{
          showLoader(0)
          //unauthorization();
        });
        break;
      default:
        break;
    }
  });
}

var authorization = (res) =>{
  if (res && res.status && res.status == 403) {
    unauthorization();
  }
  showLoader(0)
  return (res);
}

var unauthorization = () => {
  localStorage.clear();
  setAuthToken();
  window.location = '/'
}

export default {
  headers: config,
  setAuthToken: setAuthToken,
  resetAuthToken: resetAuthToken,
  apis: axiosservice,
}