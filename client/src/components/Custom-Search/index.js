import React, { Component } from "react";
import { MDBIcon } from "mdbreact";

class SearchPage extends Component {
    render() {
        return (
            <div className="input-group md-form form-sm form-1 pl-0">
                <div className="input-group-prepend">
                    <span className="input-group-text grey">
                        <MDBIcon className="text-white" icon="search" />
                    </span>
                </div>
                <input 
                className="form-control my-0 py-1" 
                type="text" 
                placeholder="Search" 
                aria-label="Search" 
                style={{border: "1px solid #ced4da", borderLeft: "0"}}
                onChange={(e) => this.props.handleInput(e.target.value)} />
            </div>
        );
    }
}

export default SearchPage; 