import React from "react";
import { Route, Switch, IndexRedirect,Redirect} from "react-router-dom";
import ExternalCollectionPage from '../../containers/Collection/ExternalCollectionPage';
class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route  path="/" component={ExternalCollectionPage}  />
        <Route exact  path="/external-collection" component={ExternalCollectionPage} />
        <Route
          render={function () {
            return <h1>Not Found</h1>;
          }}
        />
      </Switch>
    );
  }
}

export default Routes;
