import * as TYPES from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {

  case TYPES.FETCH_EXTERNAL_CONTENT:
      return { ...state, ...action.payload }

    default:
      return state;
  }
}
