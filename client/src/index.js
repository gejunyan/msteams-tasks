import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from './containers/App';
import "./index.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import configureStore from './utils/configureStore';
import 'react-notifications/lib/notifications.css';
import './assets/css/style.css';
const store = configureStore();

ReactDOM.render(
  <Router basename="/">
      <Provider store={store}>
        <Route component={App} />
      </Provider>
  </Router>,
  document.querySelector('#root')
);
