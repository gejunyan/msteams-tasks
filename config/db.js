var mongoose = require( 'mongoose' );
var SALT_WORK_FACTOR = 10;
const keys = require('./keys')

var dbURI = keys.mongoURI //'mongodb://localhost:27017/temp_2' //; 
// var dbURI = 'mongodb://'
//var dbURI =process.env.dbURI
mongoose.connect(dbURI,{ useNewUrlParser: true });

mongoose.connection.on('connected', function () {
  console.log('Mongoose connected to ' + dbURI);
});

var Promise = require("bluebird");

mongoose.Promise = require('bluebird');

mongoose.connection.on('error',function (err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose disconnected');
});
