const express = require('express')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser');
const keys = require('./config/keys');

// //Update by 23-01-2019
const app = express();
const PORT = process.env.PORT || 5000

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);

require('./server/controllers/externalCollectionRoutes')(app)

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
 const path = require('path')
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})
